package main;

import me.champeau.ld.EuroparlDetector;
import me.champeau.ld.LangDetector;
import org.apache.commons.io.FileUtils;
import utils.MapIterator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by hagai_lvi on 4/4/15.
 */
public class CBCBruteForceAttacker {

	public static final byte[] KEY_CHARACTERS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	public static final String UTF_8 = "UTF8";


	/**
	 * Using {@link CBCEncryptor#DEFAULT_MAX_INITIAL_VECTOR_LENGTH} as max initial vector size
	 * and calling {@link #attack(File, File, byte[], int)}
	 *
	 * @see #attack(File, File, byte[], int)
	 */
	public static Map<Byte, Byte> attack(File cipherTextFile, File initialVectorFile, byte[] keyCharacters)
			throws IOException {
		return attack(cipherTextFile, initialVectorFile, keyCharacters, CBCEncryptor.DEFAULT_MAX_INITIAL_VECTOR_LENGTH);
	}

	public static Map<Byte, Byte> attack(File cipherTextFile, File initialVectorFile, byte[] domain, byte[] range, int maxIVSize)
			throws IOException {

		MapIterator it = new MapIterator(domain, range);
		CBCEncryptor encryptor = new CBCEncryptor();

		EuroparlDetector detector = EuroparlDetector.getInstance();
		Set languages = new HashSet<String>();
		languages.add("en");

		Map<Byte, Byte> topKey = null;
		LangDetector.Score top = new LangDetector.Score("en", -1);
		byte[] initialVectorBytes = FileUtils.readFileToByteArray(initialVectorFile);

		if (initialVectorBytes.length > maxIVSize) {
			initialVectorBytes = Arrays.copyOfRange(initialVectorBytes, 0, maxIVSize);
		}

		int padding = 0;
		if (cipherTextFile.length() % initialVectorBytes.length != 0) {
			padding = initialVectorBytes.length - (int) cipherTextFile.length() % initialVectorBytes.length;
		}

		byte[] cipherMsgBytes = new byte[(int) cipherTextFile.length() + padding];
		new FileInputStream(cipherTextFile).read(cipherMsgBytes); // leave '0' padding from the right

		while (it.hasNext()) {

			Map<Byte, Byte> currKey = it.next();
			String deciphered = new String(encryptor.decrypt(cipherMsgBytes, currKey, initialVectorBytes, maxIVSize), UTF_8);
			LangDetector.Score curr = detector.scoreLanguages(deciphered, languages).iterator().next();
			if (curr.compareTo(top) < 0) {
				top = curr;
				topKey = currKey;
			}
		}
		return topKey;
	}

	/**
	 * return the <b><u>decryption</u></b> key.<br/>
	 * To be user when the domain and range are the same
	 */
	public static Map<Byte, Byte> attack(File cipherTextFile, File initialVectorFile, byte[] keyCharacters, int maxIVSize)
			throws IOException {
		return attack(cipherTextFile, initialVectorFile, keyCharacters, keyCharacters, maxIVSize);
	}

}
