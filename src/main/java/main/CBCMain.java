package main;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by hagai_lvi on 4/3/15.
 */
public class CBCMain {

	private final static String DECRYPTION = "Decryption";
	private final static String ENCRYPTION = "Encryption";


	public static void main(String[] args) throws IOException {
		if (args.length != 4 || (!args[0].equals(ENCRYPTION) && !args[0].equals(DECRYPTION))) {
			System.out.println(
					"Usage:\n" +
							"java -jar cbc.jar Encryption|Decryption [plainText|cipherText] [key] [iV]");
			System.exit(-1);
		}

		if (args[0].equals(ENCRYPTION)) {
			CBCEncryptor encryptor = new CBCEncryptor();
			System.out.println("Encrypting...");
			File plainTextFile = new File(args[1]);
			File keyFile = new File(args[2]);
			File initialVectorFile = new File(args[3]);
			int maxInitialVectorLength = CBCEncryptor.DEFAULT_MAX_INITIAL_VECTOR_LENGTH;
			byte[] encrypted = encryptor.encrypt(plainTextFile, keyFile, initialVectorFile, maxInitialVectorLength);

			String resFileName = FilenameUtils.removeExtension(plainTextFile.getAbsolutePath()).concat("_encrypted.").
					concat(FilenameUtils.getExtension(plainTextFile.getName()));

			FileUtils.writeByteArrayToFile(new File(resFileName), encrypted, false);
			System.out.println("Encrypted file: " + resFileName);
		} else {
			CBCEncryptor decryptor = new CBCEncryptor();
			System.out.println("Decrypting...");
			File cipherMsgFile = new File(args[1]);
			File keyFile = new File(args[2]);
			File initialVectorFile = new File(args[3]);
			int maxInitialVectorLength = CBCEncryptor.DEFAULT_MAX_INITIAL_VECTOR_LENGTH;
			byte[] decrypted = decryptor.decrypt(cipherMsgFile, keyFile, initialVectorFile, maxInitialVectorLength);

			String resFileName = FilenameUtils.removeExtension(cipherMsgFile.getAbsolutePath()).concat("_decrypted.").
					concat(FilenameUtils.getExtension(cipherMsgFile.getName()));

			FileUtils.writeByteArrayToFile(new File(resFileName), decrypted, false);
			System.out.println("Decrypted file: " + resFileName);
		}

	}
}
