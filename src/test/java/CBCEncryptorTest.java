import main.CBCEncryptor;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by hagai_lvi on 4/3/15.
 */
public class CBCEncryptorTest {

	@Test
	public void encryptTest1() throws IOException {
		encryptTest("key_example.bin", "plainMsg_example.bin", "IV_example.bin", "cipherMsg_example.bin");
	}

	@Test
	public void encryptTest2() throws IOException{
		encryptTest("part_a/myKey.bin", "part_a/Tolstoy.bin", "part_a/IV_longExample.bin", "part_a/Tolstoy_cipher.bin");
	}

	@Test
	public void encryptTest3() throws IOException{
		encryptTest("part_a/myKey.bin", "part_a/The_Wonderful_Wizard_of_OZ.bin", "part_a/IV_longExample.bin", "part_a/The_Wonderful_Wizard_of_OZ_cipher.bin");
	}

	public void encryptTest(String keyFile, String plainMsgFile, String initialVectorFile, String cipherMsgFile) throws IOException {

		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		File key = new File(classloader.getResource(keyFile).getFile());
		assertTrue("Key file doesn't exist", key.exists());

		File plainText = new File(classloader.getResource(plainMsgFile).getFile());
		assertTrue("plainText file doesn't exist", plainText.exists());

		File initVec = new File(classloader.getResource(initialVectorFile).getFile());
		assertTrue("initVec file doesn't exist", initVec.exists());

		File expectedCipherFile = new File(classloader.getResource(cipherMsgFile).getFile());
		assertTrue("expectedRes file doesn't exist", expectedCipherFile.exists());


		byte[] expectedCipher = FileUtils.readFileToByteArray(expectedCipherFile);
		byte[] actualCipher = CBCEncryptor.encrypt(plainText, key, initVec, CBCEncryptor.DEFAULT_MAX_INITIAL_VECTOR_LENGTH);

		assertArrayEquals(expectedCipher, Arrays.copyOfRange(actualCipher, 0, expectedCipher.length));
	}

	@Test
	public void decryptTest1() throws IOException {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		File cipherMsgFile = new File( classloader.getResource("cipherMsg_example.bin").getFile());
		File keyFile = new File(classloader.getResource("key_example.bin").getFile());
		File initialVectorFile = new File(classloader.getResource("IV_example.bin").getFile());
		decryptTest(cipherMsgFile, keyFile, initialVectorFile);
	}

	@Test
	public void decryptTest2() throws IOException {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		File cipherMsgFile = new File( classloader.getResource("part_a/The_Wonderful_Wizard_of_OZ_cipher.bin").getFile());
		File keyFile = new File(classloader.getResource("part_a/myKey.bin").getFile());
		File initialVectorFile = new File(classloader.getResource("part_a/IV_longExample.bin").getFile());
		decryptTest(cipherMsgFile, keyFile, initialVectorFile);
	}

	@Test
	public void decryptTest3() throws IOException {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		File cipherMsgFile = new File( classloader.getResource("cipherMsg_example.bin").getFile());
		File keyFile = new File(classloader.getResource("key_example.bin").getFile());
		File initialVectorFile = new File(classloader.getResource("IV_example.bin").getFile());
		decryptTest(cipherMsgFile, keyFile, initialVectorFile);
	}

	public void decryptTest(File cipherMsgFile, File keyFile, File initialVectorFile) throws IOException {
		assertTrue(cipherMsgFile.exists());
		assertTrue(keyFile.exists());
		assertTrue(initialVectorFile.exists());
		CBCEncryptor.decrypt(cipherMsgFile, keyFile, initialVectorFile, CBCEncryptor.DEFAULT_MAX_INITIAL_VECTOR_LENGTH);
	}
}
