package main;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import utils.CBCUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by hagai_lvi on 4/10/15.
 */
public class CBCKnownPlainTextAttacker {
	public static final int NUM_OF_LETTERS = 'z' - 'a' + 1 + 'Z' - 'A' + 1;
	public static final int MAX_IV_LENGTH = 8128;
	private static final Logger logger = Logger.getLogger(CBCKnownPlainTextAttacker.class);

	/**
	 * returns the <b><u>encryption</u></b> key
	 *
	 * @param plainMsgFile      a file containing the plain message
	 * @param cipherMsgFile     a file containing the plain cypher, should match the plain message length
	 * @param cipherTextFile    a file containing the full text
	 * @param initialVectorFile a file contain the initial vector
	 * @return a map with the <b><u>encryption</u></b> key
	 * @throws IOException if encounters problem with the files
	 */
	public static Map<Byte, Byte> attack(File plainMsgFile, File cipherMsgFile, File cipherTextFile, File initialVectorFile) throws IOException {
		Map<Byte, Byte> res = new HashMap<Byte, Byte>();

		byte[] initialVectorBytes = FileUtils.readFileToByteArray(initialVectorFile);
		if (initialVectorBytes.length > MAX_IV_LENGTH) {
			initialVectorBytes = Arrays.copyOfRange(initialVectorBytes, 0, MAX_IV_LENGTH);
		}

		//TODO what if the file is smaller then the IV length?
		// make sure cipherMsgBytes % initialVectorBytes == 0, pad with zeros if necessary
		byte[] cipherMsgBytes = new byte[(int) (cipherMsgFile.length() + initialVectorBytes.length
				- (cipherMsgFile.length() % initialVectorBytes.length))];
		new FileInputStream(cipherMsgFile).read(cipherMsgBytes);

		// make sure plainMsgBytes % initialVectorBytes == 0, pad with zeros if necessary
		byte[] plainMsgBytes = new byte[cipherMsgBytes.length];
		new FileInputStream(plainMsgFile).read(plainMsgBytes);


		for (int i = 0;
			 i + initialVectorBytes.length <= cipherMsgBytes.length && res.size() < NUM_OF_LETTERS;
			 i += initialVectorBytes.length) {
			byte[] xorred = CBCUtils.xor(
					Arrays.copyOfRange(plainMsgBytes, i, i + initialVectorBytes.length),
					initialVectorBytes
			);
			for (int j = 0; j < xorred.length && res.size() < NUM_OF_LETTERS; j++) {
				if (xorred[j] != cipherMsgBytes[i + j] && xorred[j] != 0 && cipherMsgBytes[i + j] != 0) {
					res.put(xorred[j], cipherMsgBytes[i + j]);
				}
			}
			// advance the initial vector
			initialVectorBytes = Arrays.copyOfRange(cipherMsgBytes, i, i + initialVectorBytes.length);
		}

		//try brute force attack
		if (res.size() < NUM_OF_LETTERS && cipherTextFile != null) {
			logger.info("trying brute force. current key size is " + res.size());
			long startTime = System.currentTimeMillis();
			byte[] missingKeys, missingValues;
			missingKeys = getMissingLetters(res.keySet());
			missingValues = getMissingLetters(res.values());
			if (missingKeys.length > 9) { // 9 factorial takes about 60 seconds
				logger.warn("too many missing keys: " + missingKeys.length + ". Skipping brute force");
			} else {

				Map<Byte, Byte> bruteForceRes = CBCBruteForceAttacker.
						attack(cipherTextFile, initialVectorFile, missingValues, missingKeys, initialVectorBytes.length);
				bruteForceRes = CBCUtils.inverseMap(bruteForceRes);

				res.putAll(bruteForceRes);
				logger.debug("After brute force key size is " + res.size() + ".\n" +
						"Brute force took " + (System.currentTimeMillis() - startTime) / 1000 + " seconds");
			}
		}
		return res;
	}

	/**
	 * @return The letters that are missing in the currentKey.keyset
	 */
	private static byte[] getMissingLetters(Collection<Byte> letters) {
		Set<Byte> allLetters = new HashSet<Byte>(NUM_OF_LETTERS);
		for (byte c = 'a'; c <= 'z'; c++) {
			allLetters.add(c);
		}
		for (byte c = 'A'; c <= 'Z'; c++) {
			allLetters.add(c);
		}
		allLetters.removeAll(letters);
		Byte[] tmp = allLetters.toArray(new Byte[allLetters.size()]);
		byte[] res = new byte[tmp.length];
		for (int i = 0; i < tmp.length; i++) {
			res[i] = tmp[i];
		}
		return res;
	}
}
