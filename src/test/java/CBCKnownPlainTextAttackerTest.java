import main.CBCKnownPlainTextAttacker;
import org.junit.Test;
import utils.CBCUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by hagai_lvi on 4/10/15.
 */
public class CBCKnownPlainTextAttackerTest {


	@Test
	public void test1() throws IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		File plainMsgFile = new File (classLoader.getResource("part_c2/plainMsg_example.bin").getFile());
		File cipherMsgFile = new File (classLoader.getResource("part_c2/cipherMsg_example.bin").getFile());
		File cipherTextFile = new File (classLoader.getResource("part_c2/Tolstoy_cipher.bin").getFile());
		File initialVectorFile = new File (classLoader.getResource("part_c2/IV_longExample.bin").getFile());
		File expectedKeyFile = new File(classLoader.getResource("part_c2/myKey.bin").getFile());

		attackTest(plainMsgFile, cipherMsgFile, cipherTextFile, initialVectorFile, expectedKeyFile);
	}

	private static void attackTest(File plainMsgFile, File cipherMsgFile, File cipherTextFile, File initialVectorFile, File expectedKeyFile) throws IOException {
		assertTrue(plainMsgFile.exists());
		assertTrue(cipherMsgFile.exists());
		assertTrue(initialVectorFile.exists());

		Map<Byte,Byte> res =
				CBCKnownPlainTextAttacker.attack(plainMsgFile, cipherMsgFile, cipherTextFile, initialVectorFile);
		assertEquals("Some letters are missing in the key",res.size(), CBCKnownPlainTextAttacker.NUM_OF_LETTERS);

		Map<Byte, Byte> expectedKey = CBCUtils.getEncryptKey(expectedKeyFile);
		int diff = 0;
		for (Byte b: expectedKey.keySet()){
			if ( ! expectedKey.get(b).equals(res.get(b)) ){
				System.out.println("diff: key " + (char) (b & 0xFF) + " expected " + (char) (expectedKey.get(b) & 0xFF)
						+ " got " + (char) (res.get(b) & 0xFF));
				diff++;
			}
			assertEquals("for key " + (char) (b & 0xFF) + " expected value " + (char) (expectedKey.get(b) & 0xFF) +
					" but got " + (char) (res.get(b) & 0xFF)
					,expectedKey.get(b), res.get(b));
		}
	}
}
