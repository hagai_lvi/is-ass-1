import junit.framework.TestCase;
import utils.CBCUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hagai_lvi on 4/3/15.
 */
public class KeyParserTest extends TestCase {


	public void test() throws IOException {
		Map<Byte, Byte> expectedKeys = getExpectedValuesMap();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		File key = new File(classloader.getResource("key_example.bin").getFile());
		assertTrue("Key file doesn't exist", key.exists());
		Map<Byte,Byte> actual = CBCUtils.getEncryptKey(key);
		for (Byte c: expectedKeys.keySet()){
			assertEquals(expectedKeys.get(c), actual.get(c));
		}

	}

	private Map<Byte, Byte> getExpectedValuesMap() {
		HashMap<Byte,Byte> expectedKeys = new HashMap<Byte, Byte>();
		expectedKeys.put((byte)'f', (byte)'f');
		expectedKeys.put((byte)'g', (byte)'b');
		expectedKeys.put((byte)'d', (byte)'g');
		expectedKeys.put((byte)'e', (byte)'c');
		expectedKeys.put((byte)'b', (byte)'h');
		expectedKeys.put((byte)'c', (byte)'a');
		expectedKeys.put((byte)'a', (byte)'d');
		expectedKeys.put((byte)'h', (byte)'e');
		return expectedKeys;
	}
}