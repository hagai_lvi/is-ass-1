package main;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import utils.CBCUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by hagai_lvi on 4/3/15.
 */
public class CBCEncryptor {

	public static final int DEFAULT_MAX_INITIAL_VECTOR_LENGTH = 10;
	private static Logger logger = Logger.getLogger(CBCEncryptor.class);

	public static byte[] encrypt(File plainTextFile, File keyFile, File initialVectorFile, int maxInitialVectorLength)
			throws IOException {

		if (!plainTextFile.exists()) {
			throw new FileNotFoundException("plain text file " + plainTextFile.getName() + " could not be found");
		}
		if (!keyFile.exists()) {
			throw new FileNotFoundException("key file " + keyFile.getName() + " could not be found");
		}
		if (!initialVectorFile.exists()) {
			throw new FileNotFoundException("initial vector file " + initialVectorFile.getName() + " could not be found");
		}


		byte[] initialVectorBytes = FileUtils.readFileToByteArray(initialVectorFile);
		if (initialVectorBytes.length > maxInitialVectorLength) {
			initialVectorBytes = Arrays.copyOfRange(initialVectorBytes, 0, maxInitialVectorLength);
		}

		int padding = 0;
		if (plainTextFile.length() % initialVectorBytes.length != 0) {
			padding = initialVectorBytes.length - (int) plainTextFile.length() % initialVectorBytes.length;
		}
		byte[] plainTextBytes = new byte[(int) plainTextFile.length() + padding];
		new FileInputStream(plainTextFile).read(plainTextBytes); // leave '0' padding from the right

		Map<Byte, Byte> keyMap = CBCUtils.getEncryptKey(keyFile);
		return encrypt(plainTextBytes, keyMap, initialVectorBytes, maxInitialVectorLength);
	}

	private static byte[] encrypt(byte[] plainTextBytes, Map<Byte, Byte> keyMap, byte[] initialVectorBytes, int maxInitialVectorLength) {

		if (initialVectorBytes.length > maxInitialVectorLength) {
			throw new RuntimeException("Expected initialVectorBytes to be " + maxInitialVectorLength +
					" bytes long at most");
		}
		if (plainTextBytes.length % initialVectorBytes.length != 0) {
			throw new RuntimeException("plainTextBytes % initialVectorBytes = " +
					plainTextBytes.length % initialVectorBytes.length);
		}

		byte[] res = new byte[plainTextBytes.length];
		for (int i = 0; i < plainTextBytes.length; i += initialVectorBytes.length) {
			byte[] xorred = CBCUtils.xor(
					Arrays.copyOfRange(plainTextBytes, i, i + initialVectorBytes.length),
					initialVectorBytes
			);
			for (int j = 0; j < xorred.length; j++) {
				if (keyMap.get(xorred[j]) == null) {
					res[i + j] = xorred[j];
				} else {
					res[i + j] = keyMap.get(xorred[j]);
				}
			}
			initialVectorBytes = Arrays.copyOfRange(res, i, i + initialVectorBytes.length);
		}

		return res;
	}


	public static byte[] decrypt(File cipherMsgFile, File keyFile, File initialVectorFile, int maxInitialVectorLength)
			throws IOException {

		if (!cipherMsgFile.exists()) {
			throw new FileNotFoundException("cipher text file " + cipherMsgFile.getName() + " could not be found");
		}
		if (!keyFile.exists()) {
			throw new FileNotFoundException("key file " + keyFile.getName() + " could not be found");
		}
		if (!initialVectorFile.exists()) {
			throw new FileNotFoundException("initial vector file " + initialVectorFile.getName() + " could not be found");
		}


		byte[] initialVectorBytes = FileUtils.readFileToByteArray(initialVectorFile);

		if (initialVectorBytes.length > maxInitialVectorLength) {
			initialVectorBytes = Arrays.copyOfRange(initialVectorBytes, 0, maxInitialVectorLength);
		}
		int padding = 0;
		if (cipherMsgFile.length() % initialVectorBytes.length != 0) {
			padding = initialVectorBytes.length - (int) cipherMsgFile.length() % initialVectorBytes.length;
		}

		byte[] cipherMsgBytes = new byte[(int) cipherMsgFile.length() + padding];
		new FileInputStream(cipherMsgFile).read(cipherMsgBytes); // leave '0' padding from the right

		Map<Byte, Byte> keyMap = CBCUtils.getDecryptKey(keyFile);
		return decrypt(cipherMsgBytes, keyMap, initialVectorBytes, maxInitialVectorLength);
	}

	public static byte[] decrypt(byte[] cipherMsgBytes, Map<Byte, Byte> keyMap, byte[] initialVectorBytes, int maxInitialVectorLength) {

		if (initialVectorBytes.length > maxInitialVectorLength) {
			throw new RuntimeException("initialVectorBytes.length = " + initialVectorBytes.length +
					". expected it to be at most " + maxInitialVectorLength);
		}
		if (cipherMsgBytes.length % initialVectorBytes.length != 0) {
			throw new RuntimeException("Expected cipherMsgBytes.length % initialVectorBytes.length = 0" +
					" But got " + cipherMsgBytes.length % initialVectorBytes.length);
		}


		byte[] res = new byte[cipherMsgBytes.length];
		for (int i = 0; i < cipherMsgBytes.length; i += initialVectorBytes.length) {
			byte[] decrypted = new byte[initialVectorBytes.length];
			for (int j = 0; j < decrypted.length; j++) {
				if (keyMap.get(cipherMsgBytes[i + j]) == null) {
					decrypted[j] = cipherMsgBytes[i + j];
				} else {
					decrypted[j] = keyMap.get(cipherMsgBytes[i + j]);
				}
			}

			byte[] xorred = CBCUtils.xor(decrypted, initialVectorBytes);
			for (int j = 0; j < xorred.length; j++) {
				res[i + j] = xorred[j];
			}
			initialVectorBytes = Arrays.copyOfRange(cipherMsgBytes, i, i + initialVectorBytes.length);
		}
		return removeNullPadding(res);
	}

	/**
	 * remove redundant null padding from the right
	 */
	private static byte[] removeNullPadding(byte[] bytes) {
		int size = bytes.length;
		boolean keep = true;
		for (int i = size - 1; i >= 0 & keep; i--) {
			if (bytes[i] == (byte) 0) {
				size = i;
				keep = false;
			}
		}
		byte[] res = new byte[size];

		for (int i = 0; i < res.length; i++) {
			res[i] = bytes[i];
		}
		return res;
	}


}
