package main;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import utils.CBCUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Known plain text attack
 * Created by hagai_lvi on 4/10/15.
 */
public class PlainTextAttackMain {


	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			System.out.println(
					"Usage:\n" +
							"java -jar PlainTextAttack.jar [plainTextMsg] [cipherMsg] [cipherText] [iV]");
			System.exit(-1);
		}

		File plainMsgFile = new File(args[0]);
		File cipherMsgFile = new File(args[1]);

		// the full ciphered file
		File cipherTextFile = new File(args[2]);

		File initialVector = new File(args[3]);
		System.out.println("Cracking...\n");
		Map<Byte, Byte> encryptionKey =
				CBCKnownPlainTextAttacker.attack(plainMsgFile, cipherMsgFile, cipherTextFile, initialVector);

		String res = CBCUtils.dumpMap(encryptionKey);

		String resFileName = FilenameUtils.removeExtension(cipherTextFile.getAbsolutePath()).concat("_key.").
				concat(FilenameUtils.getExtension(cipherTextFile.getName()));
		System.out.println("Output file " + resFileName + " contains the encryption key.");

		FileUtils.writeStringToFile(new File(resFileName), res, false);
	}
}
