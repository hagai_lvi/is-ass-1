This project is an assignment given in software security course in BGU.

It implements a CBC Encryptor/Decryptor, a CBC brute force attack, and a CBC known plain text attack.
It uses [jlangdetect](https://github.com/hagai-lvi/jlangdetect) for the brute force attack.  

In order to build this app: `mvn package`, three jars will be created:  
`cbc.jar` - Encryptor/Decryptor  
`CipherTextAttack.jar` - Brute force attacker  
`PlainTextAttack.jar` - Known plain text attcker  

In order to run this app run `java -jar <jar-name>` to see the usage of each of the different functionalities.  
This app requires `java 1.7` in order to run.