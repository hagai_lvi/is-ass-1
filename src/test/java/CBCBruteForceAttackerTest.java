import junit.framework.TestCase;
import main.CBCBruteForceAttacker;
import utils.CBCUtils;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;

/**
 * Created by hagai_lvi on 4/4/15.
 */
public class CBCBruteForceAttackerTest extends TestCase {

	public void test() throws IOException {
		long time = System.currentTimeMillis();

		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		File initVec = new File(classloader.getResource("IV_example.bin").getFile());
		assertTrue("initVec file doesn't exist", initVec.exists());

		File cipherFile = new File(classloader.getResource("cipherMsg_example.bin").getFile());
		assertTrue("cipherFile file doesn't exist", cipherFile.exists());

		File keyFile = new File(classloader.getResource("key_example.bin").getFile());
		assertTrue(MessageFormat.format("key file {0} does not exist", keyFile), keyFile.exists());

		Map<Byte, Byte> encryptKey = CBCUtils.getEncryptKey(keyFile);

		Map<Byte, Byte> key = CBCUtils.inverseMap(CBCBruteForceAttacker.attack(cipherFile, initVec, CBCBruteForceAttacker.KEY_CHARACTERS));

		assertEquals(encryptKey,key);
		time = System.currentTimeMillis()-time;
		System.out.println(
				MessageFormat.format(
						"Test CBCBruteForceAttackerTest Took {0} millies", time));
	}
}
