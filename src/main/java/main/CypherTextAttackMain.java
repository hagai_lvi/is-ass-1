package main;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import utils.CBCUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Activate the brute force attacker
 * Created by hagai_lvi on 4/4/15.
 */
public class CypherTextAttackMain {

	private final static String DECRYPTION = "Decryption";

	public static void main(String[] args) throws IOException {
		if (args.length != 3 || !args[0].equals(DECRYPTION)) {
			System.out.println(
					"Usage:\n" +
							"java -jar CipherTextAttack.jar Decryption [cipherText.txt] [iV.txt]");
			System.exit(-1);
		}

		File cipherTextFile = new File(args[1]);
		File initialVectorFile = new File(args[2]);
		System.out.println("Cracking...");
		Map<Byte, Byte> decryptKey = CBCBruteForceAttacker.attack(cipherTextFile, initialVectorFile, CBCBruteForceAttacker.KEY_CHARACTERS);
		Map<Byte, Byte> encryptKey = CBCUtils.inverseMap(decryptKey);

		String resFileName = FilenameUtils.removeExtension(cipherTextFile.getAbsolutePath()).concat("_key.").
				concat(FilenameUtils.getExtension(cipherTextFile.getName()));

		String res = CBCUtils.dumpMap(encryptKey);
		FileUtils.writeStringToFile(new File(resFileName), res, false);
		System.out.println("Created the key file: " + resFileName);
	}
}
