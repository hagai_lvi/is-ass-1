
#####################
# NOTE: section 3.1 #
#####################
for this section we weren't required to write a main method, instead we are showing the details below of what is required in order to make our program work with the requested parameters

In order to encrypt according to the details that are specified in in section 3.1 (8128 chars block size, 52 chars key)
one will only need to use CBCEncryptor.encrypt with the maxInitialVectorLength parameter equals to 8128.
We have implemented the Encryption/Decryption in a general form that doesn't require the key size (it derives it from the key itself).
