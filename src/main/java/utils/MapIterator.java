package utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by hagai_lvi on 4/4/15.
 */

public class MapIterator implements Iterator<Map<Byte, Byte>> {

	Iterator<int[]> it;
	private byte[] domain, range;

	/**
	 * domain and range must be of same size
	 */
	public MapIterator(byte[] domain, byte[] range) {
		this.domain = domain;
		this.range = range;
		it = new PermIterator(domain.length);

	}

	public boolean hasNext() {
		return it.hasNext();
	}

	public Map<Byte, Byte> next() {
		Map<Byte, Byte> res = new HashMap<Byte, Byte>(domain.length);
		int[] permutation = it.next();

		for (int i = 0; i < permutation.length; i++)
			res.put(domain[i], range[permutation[i]]);

		return res;
	}

	public void remove() {
		it.remove();
	}
}