package utils;

import java.io.*;
import java.util.*;

/**
 * Created by hagai_lvi on 4/3/15.
 */
public class CBCUtils {

	/**
	 * @param keyFile a key file to be parsed
	 */
	public static Map<Byte, Byte> getEncryptKey(File keyFile) throws IOException {
		if (!keyFile.exists()) {
			throw new FileNotFoundException("The key file cannot be found");
		}
		HashMap<Byte, Byte> res = new HashMap<Byte, Byte>();

		BufferedReader in = new BufferedReader(new FileReader(keyFile));
		String line;
		while ((line = in.readLine()) != null) {
			res.put((byte) line.charAt(0), (byte) line.charAt(2));
		}
		in.close();
		return res;
	}

	/**
	 * @param keyFile a key file to be parsed
	 */
	public static Map<Byte, Byte> getDecryptKey(File keyFile) throws IOException {
		if (!keyFile.exists()) {
			throw new FileNotFoundException("The key file cannot be found");
		}
		HashMap<Byte, Byte> res = new HashMap<Byte, Byte>();

		BufferedReader in = new BufferedReader(new FileReader(keyFile));
		String line;
		while ((line = in.readLine()) != null) {
			res.put(((byte) line.charAt(2)), (byte) line.charAt(0));
		}
		in.close();
		return res;
	}

	/**
	 * Inverse the map so that each k,v pair becomes v,k
	 */
	public static Map<Byte, Byte> inverseMap(Map<Byte, Byte> map) {
		Map<Byte, Byte> res = new HashMap<Byte, Byte>();
		for (Map.Entry<Byte, Byte> b : map.entrySet()) {
			res.put(b.getValue(), b.getKey());
		}
		return res;
	}

	/**
	 * the arguments are expected to be of the same length
	 */
	public static byte[] xor(byte[] text, byte[] initialVector) {
		if (text.length != initialVector.length) {
			throw new RuntimeException("text and initialvector are expected to be of the same length");
		}
		byte[] res = new byte[text.length];
		for (int i = 0; i < text.length; i++) {
			res[i] = (byte) (text[i] ^ initialVector[i]);
		}
		return res;
	}

	/**
	 * Create a string that represents the map
	 */
	public static String dumpMap(Map<Byte, Byte> map) {
		StringBuilder sb = new StringBuilder();
		List<Byte> keys = new ArrayList<Byte>(map.keySet());
		Collections.sort(keys);

		for (Byte o : keys) {
			if (sb.length() != 0) {
				sb.append("\n");
			}
			char k = (char) (o & 0xFF);
			char v = (char) (map.get(o) & 0xFF);
			sb.append(k).
					append(" ").
					append(v);
		}
		return sb.toString();
	}
}
